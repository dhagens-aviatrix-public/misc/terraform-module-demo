#Aviatrix controller vars
variable "aviatrix_admin_account" {
  type    = string
  default = "admin"
}

variable "aviatrix_admin_password" { type = string }
variable "aviatrix_controller_ip" { type = string }

#Regions
variable "region1" {
  type    = string
  default = "eu-central-1"
}

variable "region2" {
  type    = string
  default = "eu-west-1"
}

#AWS access account
variable "aws_account_number" { type = string }
variable "aws_access_key" { type = string }
variable "aws_secret_key" { type = string }
