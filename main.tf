#Controller accounts
resource "aviatrix_account" "aws" {
  account_name       = "AWS"
  cloud_type         = 1
  aws_iam            = true
  aws_account_number = var.aws_account_number
  aws_access_key     = var.aws_access_key
  aws_secret_key     = var.aws_secret_key
}

module "aws-transit-firenet-region1" {
  source  = "terraform-aviatrix-modules/aws-transit-firenet/aviatrix"
  version = "1.0.1"

  cidr             = "10.1.0.0/20"
  region           = var.region1
  aws_account_name = aviatrix_account.aws.account_name
  firewall_image   = "Fortinet FortiGate Next-Generation Firewall"
  fw_amount        = 4
}

module "aws-dev1-region1" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "1.0.0"

  spoke_name       = "dev1-region1"
  cidr             = "10.1.100.0/24"
  region           = var.region1
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region1.transit_gateway.gw_name
  ha_gw            = false
}

module "aws-prod1-region1" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "1.0.0"

  spoke_name       = "prod1-region1"
  cidr             = "10.1.101.0/24"
  region           = var.region1
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region1.transit_gateway.gw_name
}

module "aws-prod2-region1" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "1.0.0"

  spoke_name       = "prod2-region1"
  cidr             = "10.1.102.0/24"
  region           = var.region1
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region1.transit_gateway.gw_name
}

module "vpn_1_region1" {
  source  = "terraform-aviatrix-modules/aws-uservpn/aviatrix"
  version = "1.0.0"

  spoke_name       = "vpn1-region1"
  cidr             = "10.10.0.0/20"
  region           = var.region1
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region1.transit_gateway.gw_name
  vpn_gw_count     = 4
  vpn_cidr         = ["10.255.1.0/24", "10.255.2.0/24", "10.255.3.0/24", "10.255.4.0/24"]
}

module "aws-transit-firenet-region2" {
  source  = "terraform-aviatrix-modules/aws-transit-firenet/aviatrix"
  version = "1.0.1"

  cidr             = "10.2.0.0/20"
  region           = var.region2
  aws_account_name = aviatrix_account.aws.account_name
  firewall_image   = "Fortinet FortiGate Next-Generation Firewall"
  fw_amount        = 4
}

module "aws-dev1-region2" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "1.0.0"

  spoke_name       = "dev1-region2"
  cidr             = "10.2.100.0/24"
  region           = var.region2
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region2.transit_gateway.gw_name
  ha_gw            = false
}

module "aws-prod1-region2" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "1.0.0"

  spoke_name       = "prod1-region2"
  cidr             = "10.2.101.0/24"
  region           = var.region2
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region2.transit_gateway.gw_name
}

module "aws-prod2-region2" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "1.0.0"

  spoke_name       = "prod2-region2"
  cidr             = "10.2.102.0/24"
  region           = var.region2
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region2.transit_gateway.gw_name
}

module "vpn_1_region2" {
  source  = "terraform-aviatrix-modules/aws-uservpn/aviatrix"
  version = "1.0.0"

  spoke_name       = "vpn1-region2"
  cidr             = "10.20.0.0/20"
  region           = var.region2
  aws_account_name = aviatrix_account.aws.account_name
  transit_gw       = module.aws-transit-firenet-region2.transit_gateway.gw_name
  vpn_gw_count     = 4
  vpn_cidr         = ["10.255.101.0/24", "10.255.102.0/24", "10.255.103.0/24", "10.255.104.0/24"]
}

resource "aviatrix_transit_gateway_peering" "test_transit_gateway_peering" {
  transit_gateway_name1 = module.aws-transit-firenet-region1.transit_gateway.gw_name
  transit_gateway_name2 = module.aws-transit-firenet-region2.transit_gateway.gw_name
}
